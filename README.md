# lsst-ctrl-bps

[![pypi](https://img.shields.io/pypi/v/lsst-ctrl-bps.svg)](https://pypi.org/project/lsst-ctrl-bps/)
[![codecov](https://codecov.io/gh/lsst/ctrl_bps/branch/main/graph/badge.svg?token=uPLAO36lS7)](https://codecov.io/gh/lsst/ctrl_bps)

This package provides a PipelineTask execution framework for multi-node processing for the LSST Batch Production Service (BPS).

This is a Python 3 only package.

* SPIE Paper from 2022: [The Vera C. Rubin Observatory Data Butler and Pipeline Execution System](https://arxiv.org/abs/2206.14941)

PyPI: [lsst-ctrl-bps](https://pypi.org/project/lsst-ctrl-bps/)
